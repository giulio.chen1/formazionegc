// Scrivere un programma / metodo che data una stringa in input ne stampi le sole vocali. Per esempio, se si
// immette la stringa &quot;Viva Java&quot;, il programma stampa &quot;iaaa&quot;.

package it.synclab.stringhe;

import java.util.Scanner;

public class Es2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = new String();
		String str2 = new String();
		System.out.println("Inserire una stringa");
		str = scanner.nextLine();
		for(int i=0;i<str.length();i++) {
			if((str.charAt(i) == 'a')||(str.charAt(i) == 'e')||(str.charAt(i) == 'i')||(str.charAt(i) == 'o')||(str.charAt(i) == 'u'))
				str2 += str.charAt(i);
		}
		System.out.println(str2);
	}
}
