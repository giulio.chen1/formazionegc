// Scrivere un programma / metodo che data una sequenza di interi stampi la media di tutti i numeri inseriti
// che siano divisibili per tre. Per esempio, se si immettono i valori 5, 8, 9, 12, 7, 6 ,1 il risultato stampato
// dovr� essere 9. Risolvere questo esercizio senza usare array.

package it.synclab.cicli;

import java.util.Scanner;

public class Es2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = new String();
		System.out.println("Inserire una sequenza di numeri separati da uno spazio");
		str = scanner.nextLine();
		int somma = 0;
		int count = 0;
		for(int i=0;i<str.length();i++) {
			String substr = new String();
			if(str.charAt(i) != ' ') {
				substr += str.charAt(i);
				int j = i + 1;
				if(j < str.length()) {
					loop: for(;j<str.length();j++) {
						if(str.charAt(j) != ' ' ) {
							substr += str.charAt(j);
						}else break loop;
					}
				}
				
				int div = Integer.valueOf(substr);
				if(div % 3 == 0) {
					somma += div;
					count++;
				}
				i = j;
				
			}
		}
		System.out.println(somma / count);
	}
}
