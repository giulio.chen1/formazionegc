// Scrivere un programma / metodo che chiede all�utente di inserire una sequenza di caratteri (chiedendo
// prima quanti caratteri voglia inserire) e li ristampa man mano che vengono inseriti. L�intero procedimento
// (chiedere quanti caratteri voglia inserire, leggere i caratteri e man mano stamparli) dovr� essere ripetuto 5
// volte. Risolvere questo esercizio senza usare array.

package it.synclab.cicli;

import java.util.Scanner;

public class Es3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		char ch;
		int numChar;
		for(int i=0;i<5;i++) {
			System.out.println("Quanti caratteri vorresti stampare?");
			numChar = Integer.valueOf(scanner.nextLine());
			System.out.println("Inserire " + numChar + " caratteri");
			for(int j=0;j<numChar;j++) {
				ch = scanner.nextLine().charAt(0);
				System.out.println(ch);
			}
			
		}
	}
}
