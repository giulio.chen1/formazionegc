// Scrivere un programma / metodo che data una sequenza di interi stampi &quot;Tutti positivi e pari&quot; se i numeri
// inseriti sono tutti positivi e pari, altrimenti stampa &quot;NO&quot;. Risolvere questo esercizio senza usare array.

package it.synclab.cicli;
import java.util.Scanner;

public class Es1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = new String();
		String res = new String("Tutti positivi e pari");
		System.out.println("Inserire una sequenza di numeri separati da uno spazio");
		str = scanner.nextLine();
		loop: for(int i=0;i<str.length();i++) {
			String substr = new String();
			if(str.charAt(i) != ' ') {
				substr += str.charAt(i);
				int j = i + 1;
				if(j < str.length()) {
					loop2: for(;j<str.length();j++) {
						if(str.charAt(j) != ' ' ) {
							substr += str.charAt(j);
						}else break loop2;
					}
				}
				
				int div = Integer.valueOf(substr);
				if((substr.charAt(0) == '-') || (div % 2 != 0)) {
					res = "NO";
					break loop;
				}
				i = j;
				
			}
		}
		System.out.println(res);
	}
}
