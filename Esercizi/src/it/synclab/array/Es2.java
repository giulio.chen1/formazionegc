// Scrivere un metodo che verifichi se una data stringa inserita in input � palindroma, tale metodo dovr� restituire un booleano.

package it.synclab.array;
import java.util.Scanner;

public class Es2 {
	public static void main(String[] args) {
		Scanner t = new Scanner(System.in);
		System.out.println("Inserire una stringa");
		String str = t.nextLine();
		System.out.println(palindromo(str));
	}
	
	public static boolean palindromo(String str) {
		int j = str.length();
		
		for(int i=0;i<str.length();i++) {
			j--;
			if(i == str.length() / 2) {
				return true;
			}
			else if(str.charAt(i) != str.charAt(j)) {
				return false;
			}
			
		}
		return false;
	}
}
