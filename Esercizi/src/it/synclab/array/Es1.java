// Dato un array di interi, popolato casualmente, ordinarlo in ordine crescente e stampare a video il risultato di tale ordinamento.

package it.synclab.array;

public class Es1 {
	
	public static void main(String[] args) {
		
		int[] arr = new int[10];
		
		for (int i=0;i<arr.length; i++){
			int n = (int)(Math.random()*100);
			arr[i] = n;
		}
		for(int i=0;i<arr.length-1;i++) {
			if(arr[i] > arr[i+1]) {
				int a = arr[i+1];
				arr[i+1] = arr[i];
				arr[i] = a;
				for(int j=i;j>0;j--) {
					if(arr[j] < arr[j-1]) {
						int b = arr[j];
						arr[j] = arr[j-1];
						arr[j-1] = b;
					}
				}
			}
		}
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
		
		
	}
	
}
