//Scrivere un programma / metodo che preveda un array di 10 numeri interi contenente valori a piacere e ne
//stampa gli elementi secondo il seguente ordine: il primo, l�ultimo, il secondo, il penultimo, il terzo, il
//terz�ultimo, ecc.

package it.synclab.array;

public class Es5 {
	public static void main(String[] args) {
		int[] arr = new int[10];
		for(int i=0;i<arr.length;i++) {
			arr[i] = (int)(Math.random()*10);
			System.out.print(arr[i] + " ");
		}
		int[] arr2 = new int[10];
		for(int i=0;i<arr.length;i++) {
			arr2[i] = arr[i];
		}
		int k = 0;
		int j = arr2.length-1;
		for(int i=0;i<arr.length;i++) {
			if(i%2 != 0) {
				arr[i] = arr2[j];
				j--;
			}else if((i%2 == 0) && (i > 0)){
				k++;
				arr[i] = arr2[k];
			}
		}
		System.out.println();
		for(int i=0;i<arr.length;i++) {
			System.out.print(arr[i] + " ");
		}
	}
}
