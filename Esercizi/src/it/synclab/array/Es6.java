// Scrivere un programma / metodo che preveda un array di 10 numeri interi contenente valori random. Tale
// programma dovr� stampare la dicitura "Pari e dispari uguali"; se la somma dei numeri in posizioni pari
// dell�array � uguale alla somma dei numeri in posizioni dispari, altrimenti il programma dovr� stampare la
//dicitura "Pari e dispari diversi".

package it.synclab.array;

public class Es6 {
	public static void main(String[] args) {
		int[] arr = new int[10];
		for(int i=0;i<arr.length;i++) {
			arr[i] = (int)(Math.random()*10);
			System.out.print(arr[i] + " ");
		}
		int pari = 0, dispari = 0;
		for(int i=0;i<arr.length;i++) {
			if(i%2 == 0) {
				pari += arr[i];
			}else {
				dispari += arr[i];
			}
		}
		System.out.println();
		if(pari == dispari) {
			System.out.println("Pari e dispari uguali");
		}else {
			System.out.println("Pari e dispari diversi");
		}
	}
}
