// Scrivere un programma / metodo che date due sequenze di stringhe, ciascuna di 5 elementi, stampi il
// messaggio &quot;OK&quot; se almeno una stringa della prima sequenza compare anche nella seconda, altrimenti sar�
// stampato il messaggio &quot;KO&quot;. Qualora vengano trovate due stringhe uguali i confronti tra le sequenze
// devono essere interrotti.

package it.synclab.array;
import java.util.Scanner;

public class Es8 {
	public static void main(String[] args) {
		String[] arr = new String[5], arr2 = new String[5];
		Scanner scanner = new Scanner(System.in);
		String res = new String("KO");
		
		System.out.println("Inserire una prima sequenza di stringhe");
		for(int i=0;i<arr.length;i++) {
			arr[i] = scanner.nextLine();
		}
		System.out.println("Inserire una seconda sequenza di stringhe");
		for(int i=0;i<arr.length;i++) {
			arr2[i] = scanner.nextLine();
		}
		int k=0;
		loop: for(int i=0;i<arr.length;i++) {
			for(int j=0;j<arr2.length;j++) {
				if(arr[i].equals(arr2[j])) {
					res = "OK";
					k++;
					if(k == 2) {
						break loop;
					}
				}
			}
		}
		System.out.println(res);
	}
}
