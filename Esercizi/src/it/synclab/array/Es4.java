// Data una matrice effettuare la trasposta della stessa.

package it.synclab.array;

public class Es4 {
	public static void main(String[] args) {
		int[][] matrix = new int[4][4];
		for(int i=0;i<matrix.length;i++) {
			for(int j=0;j<matrix[i].length;j++) {
				matrix[i][j] = (int)(Math.random()*10);
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println();
		
		int k=0;
		for(int i=0;i<matrix.length;i++) {
			for(int j=k;j<matrix[i].length;j++) {
				int comodo = matrix[i][j];
				matrix[i][j] = matrix[j][i];
				matrix[j][i] = comodo;
			}
			k++;
			System.out.println();
		}
		
		for(int i=0;i<matrix.length;i++) {
			for(int j=0;j<matrix[i].length;j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}
}
